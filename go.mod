module gitlab.com/3stadt/twitchchat2led

go 1.14

require (
	github.com/gempir/go-twitch-irc/v2 v2.3.0
	github.com/gorilla/websocket v1.4.1
	github.com/ilyakaznacheev/cleanenv v1.2.1
)
