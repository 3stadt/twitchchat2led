# Purpose

Get notified of new twitch chat messages via a blinking LED.  

# Why?

I (3stadt) am a hobbyist twitch streamer. My stream has rather low chat activity, so I tend to react to chat messages
too late. With a LED blinking right above my monitor I'm instantly notified and can react accordingly. That's it. 

# How it works

There are two main components:

- A websocket server that also listens in a twitch channel for new messages
- A websocket client on a ESP8266 Chip that blinks when it receives a websocket message

The Server language is GO, and can run basically anywhere you want. The ESP8266 uses LUA via NodeMCU as language.

I wanted to decouple the parts, you could also put everything on a RPi and skip the websocket part, that's up to you.
With the ESP8266 though, you only need a power source and are good to go, no worries about rebooting, and the power 
consumption is pretty much negligible.

# How to set up

The hardware/ESP8266 part is the only hard part in this project.    
If you want to use the exact same setup as I do, here's a part list:

- 1x [NodeMCU Lua Lolin V3 Module ESP8266](https://duckduckgo.com/?q=NodeMCU+Lua+Lolin+V3+Module+ESP8266&atb=v144-1&iax=shopping&ia=shopping) from [AZ Delivery](https://www.az-delivery.de/products/copy-of-nodemcu-lua-amica-v2-modul-mit-esp8266-12e)
- 1x RGB LED
- 3x 1k Ohm Resistor
- 4x F2F dupont wires
- 1x breadboard

I got the wires, resistors, RGB LED and breadboard from an Arduino starter kit.
You can use whatever you want though. ^^

Here're the schematics and a photo of my setup:

![](.readme/schematic.png?raw=true)

[![](.readme/circuit_photo.jpg?raw=true)](.readme/circuit_photo_big.jpg?raw=true)

> Pay attention to the ground connection in the photo to correctly get the colors connected.   
> The additional 2nd black ground wire is not necessary. I just wanted it that way. 

With everything wired up, you need to flash [NodeMCU](https://nodemcu.readthedocs.io/) to the ESP8266.
I've used the [cloud service](https://nodemcu-build.com/), but other options are [available in the docs](https://nodemcu.readthedocs.io/en/master/build/).
Either way, **make sure to include the websocket package**!

Download or clone the `init.lua` from this repo, change the Wi-Fi data and the websocket URL. The websocket URL is the
URL your server will be running at. So this won't be localhost, but either a domain or the IP address of your PC in case
you want to run this in your local network.

Upload the `init.lua` to your ESP8266. I suggest [ESPlorer](https://esp8266.ru/esplorer/) for this.

Next, either build the go server yourself or download, extract and run the binary from the [latest successful artifact](https://gitlab.com/3stadt/twitchchat2led/-/jobs/artifacts/master/download?job=build). Don't forget to set your own values in the `config.yml`.

If you want to stop the LED from blinking because you've read the current message, use [stream deck](https://www.elgato.com/en/gaming/stream-deck), a key binding on your mouse/keyboard, [AutoIt](https://www.autoitscript.com/site/) with [WinHttpRequest](https://beamtic.com/http-requests-autoit) or whatever you like to send a GET request to `http://<YOUR URL>/stopblink`.

You can also pause the blinker for a time period: `http://<YOUR URL>/pauseblink/int/dur` where `int` is a number and `dur` is `seconds`, `minutes` or `hours`. Examples: `http://localhost:8000/pauseblink/15/minutes` or `http://localhost:8000/pauseblink/1/hour` - singular duration works as well. 

To resume the blinker before the set time is reached use `http://<YOUR URL>/resumeblink`.

To see if the messages from the server are actually working, use [WebSocket Weasel](https://addons.mozilla.org/en-US/firefox/addon/websocket-weasel/) for Firefox or [Simple WebSocket Client](https://chrome.google.com/webstore/detail/simple-websocket-client/pfdhoblngboilpfeibdedpjgfnlcodoo) for Chrome. Make sure to use `ws` instead of `wss` as protocol!

That's it!
