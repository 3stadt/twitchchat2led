wifi.setmode(wifi.STATION)
station_cfg={}
station_cfg.ssid = "SSID"
station_cfg.pwd = "PASSWORD"
station_cfg.save = false
wifi.sta.config(station_cfg)

isConnecting = false

local ws = websocket.createClient()
local reconnectTimer = tmr.create()
local RLED = 7
local GLED = 6
local BLED = 5
local wsAddress = 'ws://CHANGEME:8000/ws'

function led(r, g, b) -- 255 based values: value*4.0117
    pwm.setduty(RLED, r)
    pwm.setduty(GLED, g)
    pwm.setduty(BLED, b)
end

function white()
    led(1023, 1023, 1023)
end

function pink()
    led(1023, 0, 850)
end

function red()
    led(1023, 0, 0)
end

function green()
    led(0, 1023, 0)
end

function blue()
    led(0, 0, 1023)
end

function off()
    led(0,0,0)
end

pwm.setup(RLED, 1000, 1023)
pwm.setup(GLED, 1000, 1023)
pwm.setup(BLED, 1000, 1023)
pwm.start(RLED)
pwm.start(GLED)
pwm.start(BLED)

led(0,0,0)

ws:config({headers={['User-Agent']='NodeMCU'}})

ws:on("connection", function(ws)
    print('got ws connection')
    reconnectTimer:unregister()
    off()
end)

ws:on("close", function(_, status)
    print('connection closed', status)
    ws = nil -- required to Lua gc the websocket client
    red()
    if(isConnecting == false)
    then
        reconnect()
    end
end)

ws:on("receive", function(_, msg, opcode)
    if(msg == "blink" or msg == "white")
    then
        white()
        tmr.delay(90000)
        off()
    elseif(msg == "red")
    then
        red()
        tmr.delay(90000)
        off()
    elseif(msg == "green")
    then
        green()
        tmr.delay(90000)
        off()
    elseif(msg == "blue")
    then
        blue()
        tmr.delay(90000)
        off()
    elseif(msg == "pink")
    then
        pink()
        tmr.delay(90000)
        off()
    end
end)

function reconnect()
    isConnecting = true
    reconnectTimer = tmr.create()
    reconnectTimer:register(10000, tmr.ALARM_SINGLE, function (t)
        print('reconnecting...')
        ws = nil
        ws = websocket.createClient()
        ws:config({headers={['User-Agent']='NodeMCU'}})
        ws:connect(wsAddress)
        isConnecting = false
        reconnectTimer:unregister()
    end)
    reconnectTimer:start()
end

-- wait for IP from fritz.box

local ipTimer = tmr.create()

ipTimer:register(1000, tmr.ALARM_AUTO, function (t)
    isConnecting = true
    if wifi.sta.getip()==nil then
        print(" Waiting for IP address...")
    else
        print("IP address is "..wifi.sta.getip())
        t:unregister()
        ws:connect(wsAddress)
        isConnecting = false
    end
end)

ipTimer:start()
