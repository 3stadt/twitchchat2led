package main

import (
	"fmt"
	"github.com/gempir/go-twitch-irc/v2"
	"regexp"
	"strings"
)

func connectTwitchIRC(c *Config) error {
	client := twitch.NewAnonymousClient() // read only

	client.OnPrivateMessage(func(m twitch.PrivateMessage) {
		if isIgnoredUser(m.User.Name, c.IgnoreUsers) ||
			isIgnoredMessagePattern(m.Message, c.IgnoreMessagesPatterns) ||
			c.isDirectMessage(m.Message) {
			return
		}
		blinker.mu.Lock()
		blinker.color = c.MessageColor
		if messageContains(m.Message, c.Highlights) {
			blinker.color = c.HighlightColor
		}
		blinker.active = true
		blinker.mu.Unlock()
	})

	client.Join(c.Channel)

	return client.Connect()
}

// isDirectMessage checks if a word in the message is prefixed by "@"
// always returns false on streamer mention
func (c *Config) isDirectMessage(message string) bool {
	message = strings.ToLower(message)
	if strings.Contains(message, fmt.Sprintf("@%s", strings.ToLower(c.Channel))) {
		return false
	}
	words := strings.Split(message, " ")
	for _, word := range words {
		if !strings.HasPrefix(word, "@") {
			continue
		}
		return true
	}
	return false
}

func isIgnoredMessagePattern(message string, patterns []string) bool {
	message = strings.ToLower(message)
	for _, pattern := range patterns {
		matched, err := regexp.Match(pattern, []byte(message))
		if err != nil {
			fmt.Printf("Error matching pattern %q: %s", pattern, err)
			continue
		}
		if matched {
			return true
		}
	}
	return false
}

func isIgnoredUser(name string, users []string) bool {
	name = strings.ToLower(name)
	for _, user := range users {
		if name == strings.ToLower(user) {
			return true
		}
	}
	return false
}

func messageContains(message string, highlights []string) bool {
	message = strings.ToLower(message)
	for _, highlight := range highlights {
		if strings.Contains(message, strings.ToLower(highlight)) {
			return true
		}
	}
	return false
}
