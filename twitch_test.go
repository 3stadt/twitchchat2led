package main

import "testing"

func TestConfig_isDirectMessage(t *testing.T) {
	type fields struct {
		Secret                 string
		Listen                 string
		Channel                string
		Highlights             []string
		HighlightColor         string
		MessageColor           string
		IgnoreOfflineMessages  bool
		IgnoreMessagesPatterns []string
		IgnoreUsers            []string
	}
	type args struct {
		message string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "regular message",
			fields: fields{
				Channel:                "FooBar",
			},
			args: args{message: "Hey, what's up everyone?"},
			want: false,
		},
		{
			name: "direct mention",
			fields: fields{
				Channel:                "FooBar",
			},
			args: args{message: "Hey, what's up @foobar?"},
			want: false,
		},
		{
			name: "indirect mention",
			fields: fields{
				Channel:                "FooBar",
			},
			args: args{message: "Hey @someone, what's up with @foobar?"},
			want: false,
		},
		{
			name: "no mention",
			fields: fields{
				Channel:                "FooBar",
			},
			args: args{message: "Hey @someone, what's up?"},
			want: true,
		},
		{
			name: "no mention when no @ in name",
			fields: fields{
				Channel:                "FooBar",
			},
			args: args{message: "Hey @someone, what's up with Foobar?"},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				Secret:                 tt.fields.Secret,
				Listen:                 tt.fields.Listen,
				Channel:                tt.fields.Channel,
				Highlights:             tt.fields.Highlights,
				HighlightColor:         tt.fields.HighlightColor,
				MessageColor:           tt.fields.MessageColor,
				IgnoreOfflineMessages:  tt.fields.IgnoreOfflineMessages,
				IgnoreMessagesPatterns: tt.fields.IgnoreMessagesPatterns,
				IgnoreUsers:            tt.fields.IgnoreUsers,
			}
			if got := c.isDirectMessage(tt.args.message); got != tt.want {
				t.Errorf("isDirectMessage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isIgnoredMessagePattern(t *testing.T) {
	type args struct {
		message  string
		patterns []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "example is ignored",
			args: args{
				message:  "!uptime today",
				patterns: []string{"^!.*"},
			},
			want: true,
		},
		{
			name: "example is not ignored",
			args: args{
				message:  "what's the !uptime today?",
				patterns: []string{"^!.*"},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isIgnoredMessagePattern(tt.args.message, tt.args.patterns); got != tt.want {
				t.Errorf("isIgnoredMessagePattern() = %v, want %v", got, tt.want)
			}
		})
	}
}
