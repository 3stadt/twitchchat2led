package main

import (
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	hub              *Hub
	blinker          *blink
	cfg              Config
	stopBlinkRoute   = "/stopblink"
	pauseBlinkRoute  = "/pauseblink/"
	resumeBlinkRoute = "/resumeblink"
	wsRoute          = "/ws"
	timeUnits        = map[string]time.Duration{
		"seconds": time.Second,
		"second":  time.Second,
		"minutes": time.Minute,
		"minute":  time.Minute,
		"hours":   time.Hour,
		"hour":    time.Hour,
	}
)

type Config struct {
	Secret                 string   `yaml:"secret"`
	Listen                 string   `yaml:"listen"`
	Channel                string   `yaml:"channel"`
	Highlights             []string `yaml:"highlights"`
	HighlightColor         string   `yaml:"highlightColor"`
	MessageColor           string   `yaml:"messageColor"`
	IgnoreOfflineMessages  bool     `yaml:"ignoreOfflineMessages"`
	IgnoreMessagesPatterns []string `yaml:"ignoreMessagesPattern"`
	IgnoreUsers            []string `yaml:"ignoreUsers"`
}

func init() {
	hub = newHub()
	blinker = &blink{
		mu:     &sync.Mutex{},
		active: false,
	}
	err := cleanenv.ReadConfig("config.yml", &cfg)
	if err != nil {
		fmt.Printf("Could not read config: %s\n", err)
		os.Exit(1)
	}
}

func main() {
	go hub.run()
	go func() {
		err := connectTwitchIRC(&cfg)
		if err != nil {
			setBlinkerActiveFalse()
			fmt.Printf("Twitch error: %s\n", err)
		}
	}()
	go startBlinker()

	if cfg.Secret != "" {
		stopBlinkRoute += fmt.Sprintf("/%s", cfg.Secret)
		pauseBlinkRoute += fmt.Sprintf("%s/", cfg.Secret)
		resumeBlinkRoute += fmt.Sprintf("/%s", cfg.Secret)
		wsRoute += fmt.Sprintf("/%s", cfg.Secret)
	}

	http.HandleFunc(wsRoute, func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	http.HandleFunc(stopBlinkRoute, stopBlink)
	// actually expects the URL to have pauseBlinkRoute/int/unit
	// Example: http://localhost:8080/pauseblink/5/minutes
	http.HandleFunc(pauseBlinkRoute, pauseBlink)
	http.HandleFunc(resumeBlinkRoute, resumeBlink)
	fmt.Printf("Listening on %s\n", cfg.Listen)
	err := http.ListenAndServe(cfg.Listen, nil)
	if err != nil {
		fmt.Println("ListenAndServe: ", err)
		os.Exit(1)
	}
}

func resumeBlink(w http.ResponseWriter, r *http.Request) {
	blinker.mu.Lock()
	blinker.pauseBlinkEnd = nil
	blinker.mu.Unlock()
}

func pauseBlink(w http.ResponseWriter, r *http.Request) {
	timeToPause := strings.Split(strings.TrimSuffix(strings.TrimPrefix(r.URL.Path, pauseBlinkRoute), "/"), "/")
	if len(timeToPause) != 2 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	t, err := strconv.ParseUint(timeToPause[0], 10, 0)
	if err != nil || t < 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if dur, ok := timeUnits[strings.ToLower(timeToPause[1])]; ok {
		blinker.mu.Lock()
		blinker.active = false
		pauseBlinkEnd := time.Now().Add(time.Duration(t) * dur)
		blinker.pauseBlinkEnd = &pauseBlinkEnd
		blinker.mu.Unlock()
	}
}

func stopBlink(w http.ResponseWriter, r *http.Request) {
	setBlinkerActiveFalse()
	w.WriteHeader(http.StatusNoContent)
}

func setBlinkerActiveFalse() {
	blinker.mu.Lock()
	blinker.active = false
	blinker.mu.Unlock()
}
