#!/usr/bin/env sh

mkdir -p dist/windows
mkdir -p dist/linux
mkdir -p dist/mac

go mod tidy
GOOS=windows ARCH=amd64 go build -o dist/windows/twitchChat2LED.exe .
GOOS=linux ARCH=amd64 go build -o dist/linux/twitchChat2LED .
GOOS=darwin ARCH=amd64 go build -o dist/mac/twitchChat2LED .

cp config.yml.dist dist/windows/config.yml
cp config.yml.dist dist/linux/config.yml
cp config.yml.dist dist/mac/config.yml
